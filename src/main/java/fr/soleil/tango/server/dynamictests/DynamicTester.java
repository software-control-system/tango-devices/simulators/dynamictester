package fr.soleil.tango.server.dynamictests;

import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.StateMachineBehavior;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.util.TypeConversionUtil;

@Device
public final class DynamicTester {

    private class MyAttributeBehavior implements IAttributeBehavior {
        private AttributeConfiguration ac;
        private AttributeValue av = null;
        private boolean isNull = false;

        public MyAttributeBehavior(final AttributeConfiguration ac, final boolean isNull) throws DevFailed {
            this.ac = ac;
            if (ac.getType().equals(String.class) || ac.getType().equals(String[].class)) {
                this.av = new AttributeValue("None");
            } else {
                this.av = new AttributeValue(0.0d);
            }
            this.isNull = isNull;
        }

        @Override
        public AttributeConfiguration getConfiguration() throws DevFailed {
            return this.ac;
        }

        public void setConfiguration(final AttributeConfiguration ac) throws DevFailed {
            this.ac = ac;
        }

        @Override
        public AttributeValue getValue() throws DevFailed {
            if (this.isNull) {
                throw new DevFailed();
            }
            return this.av;
        }

        @Override
        public void setValue(final AttributeValue value) throws DevFailed {
            this.av = value;
        }

        @Override
        public StateMachineBehavior getStateMachine() throws DevFailed {
            // NOT USE
            return null;
        }

    }

    public static void main(final String[] args) {

        // ServerManager.getInstance().addClass("Test", DynamicTester.class);
        ServerManager.getInstance().start(args, DynamicTester.class);
    }

    @DynamicManagement
    private DynamicManager dynMngt;

    public void setDynMngt(final DynamicManager dynMngt) {
        this.dynMngt = dynMngt;
    }

    @Status
    private String status = "";

    public String getStatus() {
        return this.status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    @State
    private DeviceState state = DeviceState.INIT;

    public DeviceState getState() {
        return this.state;
    }

    public void setState(final DeviceState state) {
        this.state = state;
    }

    @Init
    public void initDevice() throws DevFailed {
        this.status = "Init done";
        this.state = DeviceState.ON;

        this.addAttribute(new String[] { "double_scalar_rw", "5", "0", "3" });
    }

    @Delete
    public void deleteDevice() throws DevFailed {
        this.dynMngt.clearAll();
    }

    @Command(inTypeDesc = "String array : attribute name, numeric tango type , numeric format and numeric writable")
    public void addAttribute(final String[] args) throws DevFailed {
        if (args.length != 4) {
            DevFailedUtils.throwDevFailed("4 arguments needed, check the argin description");
        }
        final AttributeConfiguration ac = new AttributeConfiguration();
        ac.setName(args[0]);

        Integer type = null;
        AttrDataFormat format = null;
        AttrWriteType write = null;

        try {
            type = Integer.parseInt(args[1]);
        } catch (final NumberFormatException e) {
            type = TangoUtil.TYPE_MAP.get(args[1].toUpperCase());
        }
        if (type == null) {
            DevFailedUtils.throwDevFailed("Type is not correct");
        }

        try {
            format = AttrDataFormat.from_int(Integer.parseInt(args[2]));
        } catch (final NumberFormatException e) {
            format = TangoUtil.FORMAT_MAP.get(args[2].toUpperCase());
        }
        if (format == null) {
            DevFailedUtils.throwDevFailed("Format is not correct");
        }
        try {
            write = AttrWriteType.from_int(Integer.parseInt(args[3]));
        } catch (final NumberFormatException e) {
            write = TangoUtil.WRITABLE_MAP.get(args[3].toUpperCase());
        }
        if (write == null) {
            DevFailedUtils.throwDevFailed("Writeable is not correct");
        }

        ac.setTangoType((type), format);
        ac.setWritable(write);
        this.dynMngt.addAttribute(new MyAttributeBehavior(ac, false));
    }

    @Command(inTypeDesc = "String array : attribute name, numeric tango type , numeric format and numeric writable")
    public void addNullAttribute(final String[] args) throws DevFailed {
        if (args.length != 4) {
            DevFailedUtils.throwDevFailed("4 arguments needed, check the argin description");
        }

        Integer type = null;
        AttrDataFormat format = null;
        AttrWriteType write = null;

        try {
            type = Integer.parseInt(args[1]);
        } catch (final NumberFormatException e) {
            type = TangoUtil.TYPE_MAP.get(args[1].toUpperCase());
        }
        if (type == null) {
            DevFailedUtils.throwDevFailed("Type is not correct");
        }

        try {
            format = AttrDataFormat.from_int(Integer.parseInt(args[2]));
        } catch (final NumberFormatException e) {
            format = TangoUtil.FORMAT_MAP.get(args[2].toUpperCase());
        }
        if (format == null) {
            DevFailedUtils.throwDevFailed("Format is not correct");
        }
        try {
            write = AttrWriteType.from_int(Integer.parseInt(args[3]));
        } catch (final NumberFormatException e) {
            write = TangoUtil.WRITABLE_MAP.get(args[3].toUpperCase());
        }
        if (write == null) {
            DevFailedUtils.throwDevFailed("Writeable is not correct");
        }

        final AttributeConfiguration ac = new AttributeConfiguration();
        ac.setName(args[0]);
        ac.setTangoType(type, format);
        ac.setWritable(write);
        this.dynMngt.addAttribute(new MyAttributeBehavior(ac, true));
    }

    @Command(inTypeDesc = "The attribute name")
    public void removeAttribute(final String attrName) throws DevFailed {
        if (attrName == null || attrName.trim().isEmpty()) {
            DevFailedUtils.throwDevFailed("the attribute name can't be empty");
        }
        this.dynMngt.removeAttribute(attrName);
    }

    @Command()
    public void removeAttributes() throws DevFailed {
        this.dynMngt.clearAttributes();
    }

    @Command(inTypeDesc = "String array : attribute name, numeric tango type , numeric format and numeric writable")
    public void changeAttribute(final String[] args) throws DevFailed {
        if (args.length != 4) {
            DevFailedUtils.throwDevFailed("4 arguments needed, check the argin description");
        }

        Integer type = null;
        AttrDataFormat format = null;
        AttrWriteType write = null;

        try {
            type = Integer.parseInt(args[1]);
        } catch (final NumberFormatException e) {
            type = TangoUtil.TYPE_MAP.get(args[1].toUpperCase());
        }
        if (type == null) {
            DevFailedUtils.throwDevFailed("Type is not correct");
        }

        try {
            format = AttrDataFormat.from_int(Integer.parseInt(args[2]));
        } catch (final NumberFormatException e) {
            format = TangoUtil.FORMAT_MAP.get(args[2].toUpperCase());
        }
        if (format == null) {
            DevFailedUtils.throwDevFailed("Format is not correct");
        }
        try {
            write = AttrWriteType.from_int(Integer.parseInt(args[3]));
        } catch (final NumberFormatException e) {
            write = TangoUtil.WRITABLE_MAP.get(args[3].toUpperCase());
        }
        if (write == null) {
            DevFailedUtils.throwDevFailed("Writeable is not correct");
        }

        final MyAttributeBehavior mab = (MyAttributeBehavior) this.dynMngt.getAttribute(args[0]);
        final AttributeConfiguration ac = mab.getConfiguration();

        ac.setWritable(write);
        ac.setTangoType(type, format);
        if (format.equals(AttrDataFormat.SPECTRUM)) {
            ac.setMaxX(Integer.MAX_VALUE);
            ac.setMaxY(0);
            mab.getValue().setValue(TypeConversionUtil.castToType(ac.getType(), mab.getValue().getValue()));
        } else if (format.equals(AttrDataFormat.IMAGE)) {
            ac.setMaxX(Integer.MAX_VALUE);
            ac.setMaxY(Integer.MAX_VALUE);
            mab.getValue().setValue(TypeConversionUtil.castToType(ac.getType(), mab.getValue().getValue()));
        } else {
            if (mab.getValue().getValue().getClass().isArray()) {
                mab.getValue().setValue(0);
            } else {
                mab.getValue().setValue(TypeConversionUtil.castToType(ac.getType(), mab.getValue().getValue()));
            }
            ac.setMaxX(1);
            ac.setMaxY(0);
        }
        mab.setConfiguration(ac);
        this.removeAttribute(args[0]);
        this.dynMngt.addAttribute(mab);
    }

    @Command(inTypeDesc = "String array : attribute name and numeric tango type")
    public void changeTypeAttribute(final String[] args) throws DevFailed {
        if (args.length != 2) {
            DevFailedUtils.throwDevFailed("2 arguments needed, check the argin description");
        }

        Integer type = null;

        try {
            type = Integer.parseInt(args[1]);
        } catch (final NumberFormatException e) {
            type = TangoUtil.TYPE_MAP.get(args[1].toUpperCase());
        }
        if (type == null) {
            DevFailedUtils.throwDevFailed("Type is not correct");
        }

        final MyAttributeBehavior mab = (MyAttributeBehavior) this.dynMngt.getAttribute(args[0]);
        final AttributeConfiguration ac = mab.getConfiguration();
        final AttrDataFormat format = ac.getFormat();

        ac.setTangoType(type, format);
        if (format.equals(AttrDataFormat.SCALAR)) {
            mab.getValue().setValue(TypeConversionUtil.castToType(ac.getType(), mab.getValue().getValue()));
        } else {
            mab.getValue().setValue(TypeConversionUtil.castToArray(ac.getType(), mab.getValue().getValue()));
        }
        mab.setConfiguration(ac);
        this.removeAttribute(args[0]);
        this.dynMngt.addAttribute(mab);
    }

    @Command(inTypeDesc = "String array : attribute name and numeric writable")
    public void changeWritableAttribute(final String[] args) throws DevFailed {
        if (args.length != 2) {
            DevFailedUtils.throwDevFailed("2 arguments needed, check the argin description");
        }
        AttrWriteType write = null;

        try {
            write = AttrWriteType.from_int(Integer.parseInt(args[1]));
        } catch (final NumberFormatException e) {
            write = TangoUtil.WRITABLE_MAP.get(args[1].toUpperCase());
        }

        final MyAttributeBehavior mab = (MyAttributeBehavior) this.dynMngt.getAttribute(args[0]);
        final AttributeConfiguration ac = mab.getConfiguration();
        ac.setWritable(write);
        mab.setConfiguration(ac);
        this.removeAttribute(args[0]);
        this.dynMngt.addAttribute(mab);
    }

    @Command(inTypeDesc = "String array : attribute name and numeric format")
    public void changeFormatAttribute(final String[] args) throws DevFailed {
        if (args.length != 2) {
            DevFailedUtils.throwDevFailed("2 arguments needed, check the argin description");
        }

        AttrDataFormat format = null;
        try {
            format = AttrDataFormat.from_int(Integer.parseInt(args[1]));
        } catch (final NumberFormatException e) {
            format = TangoUtil.FORMAT_MAP.get(args[1].toUpperCase());
        }
        if (format == null) {
            DevFailedUtils.throwDevFailed("Format is not correct");
        }

        final MyAttributeBehavior mab = (MyAttributeBehavior) this.dynMngt.getAttribute(args[0]);
        final AttributeConfiguration ac = mab.getConfiguration();
        ac.setTangoType(ac.getTangoType(), format);
        if (format.equals(AttrDataFormat.SPECTRUM)) {
            ac.setMaxX(Integer.MAX_VALUE);
            ac.setMaxY(0);
            mab.getValue().setValue(TypeConversionUtil.castToArray(ac.getType(), mab.getValue().getValue()));
        } else if (format.equals(AttrDataFormat.IMAGE)) {
            ac.setMaxX(Integer.MAX_VALUE);
            ac.setMaxY(Integer.MAX_VALUE);
            mab.getValue().setValue(TypeConversionUtil.castToArray(ac.getType(), mab.getValue().getValue()));
        } else {
            ac.setMaxX(1);
            ac.setMaxY(0);
            mab.getValue().setValue(TypeConversionUtil.castToType(ac.getType(), mab.getValue().getValue()));
        }
        mab.setConfiguration(ac);
        this.removeAttribute(args[0]);
        this.dynMngt.addAttribute(mab);
    }

}
